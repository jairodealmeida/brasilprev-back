package com.brasilprev.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.brasilprev.models.Customer;
import com.brasilprev.models.User;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin()
//@CrossOrigin(origins = "https://stormy-journey-60924.herokuapp.com")
@RestController
@RequestMapping({ "/customers" })

/**
@class TesteController
Class to test sample controller and deployment aprove
*/

public class CustomerController {

	private List<Customer> customers = createList();

	@GetMapping(produces = "application/json")
	public List<Customer> firstPage() {
		return customers;
	}

	/*@GetMapping(produces = "application/json")
	@RequestMapping({ "/validateLogin" })
	public User validateLogin() {
		return new User("User successfully authenticated");
	}*/

	@DeleteMapping(path = { "/{id}" })
	public Customer delete(@PathVariable("id") String id) {
		Customer deleted = null;
		System.out.println(customers);
		for (Customer emp : customers) {
			if (emp.getId().equals(id)) {
				customers.remove(emp);
				deleted = emp;
				break;
			}
		}
		return deleted;
	}

	@PostMapping
	public Customer create(@RequestBody Customer user) {
		customers.add(user);
		System.out.println(customers);
		return user;
	}

	private static List<Customer> createList() {
		List<Customer> tempCustomers = new ArrayList<>();
		Customer emp1 = new Customer();
		emp1.setName("Andressa de Almeida Christo");
		emp1.setRegistry("22194497880");
		emp1.setId("1");
		emp1.setAddress("Rua Hilda C de Avelar Garcia, 122, Assis SP");

		Customer emp2 = new Customer();
		emp2.setName("Jairo de Almeida");
		emp2.setRegistry("22192897880");
		emp2.setId("2");
		emp2.setAddress("Rua Hilda C de Avelar Garcia, 122, Assis SP");
		tempCustomers.add(emp1);
		tempCustomers.add(emp2);
		return tempCustomers;
	}

}
