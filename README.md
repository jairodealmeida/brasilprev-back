# BrasilPrev Challenge #

## Global Details ##
I'm sending the challenge that the company proposed to me
There are two repositories, as my profile is FullStack I created two
One for the frontend and one for the backend, both can be accessed by commands
There are README.md files that document the two fronts

### BackEnd - Solução SpringBot - Access###
BackEnd: Onde estão os Micro Serviços Spring Data REST desenvolvido em JAVA 8, para post, put e delete,  Uso de Maven e Docker  e autenticação.
Bitbucket do BackEnd :   git clone https://jairodealmeida@bitbucket.org/jairodealmeida/brasilprev-back.git
O servidor que eu utilizei para o Cloud do patamar, SpringBoot esta no Heroku
Microserviços : https://boiling-earth-70825.herokuapp.com/


### com.brasilprev.SpringBootHelloWorldApplication class ###
Responsable to init  SpringApplication (have main args)


### com.brasilprev.model.Customer class ###
Class that comes from a serializable object from the frontend, has hash and compare attributs

### com.brasilprev.controller.CustomerController class ###
Is the class in charge of obtaining the rest methods, POST, PUT and Delete
to manibulate a Customer objects
Have cors to https://stormy-journey-60924.herokuapp.com too


## To remote DEV developers ##

Command to execute no PROD SpringBoot APP
```
./mvnw spring-boot:run
```

Command to export deployable JAR
```
java -jar .target/BrasilPrevDesafio-0.0.1-SNAPSHOT.jar
```
Docker anotation to generic export jar in deploy
root/Dockerfile
```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} BrasilPrevDesafio-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/BrasilPrevDesafio-0.0.1-SNAPSHOT.jar"]
```
## To enable CORS ##
Controllers set this JAVA annotation
@CrossOrigin(origins = "https://stormy-journey-60924.herokuapp.com")

## Heroku Annotations Steps ##

* Install Heroku Client
* Commit changes to Git end Push       
* heroku create  (Create heroku cloud )
* heroku logs --tail     (Log It)
* heroku open (Open remote app)
* git push heroku master (Push changes to Heroky GIt and Autodeploy it)
* heroku ps:scale web=1 (Define Web)
